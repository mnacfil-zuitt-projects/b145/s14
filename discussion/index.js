// displays the message
console.log("Hello from JS");
// single-line comment
/* multi-line comment*/

let myVariable;
// (=) assignment operator
let clientName = "Juan Dela Cruz";
let contactNumber = 9951446335;

let greetings;
console.log(greetings);
console.log(clientName);
console.log(contactNumber);
let pangalan = "John Doe"
console.log(pangalan);

let spouse = null;

console.log(spouse);

// Arrays
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javscript"];

console.log(bootcampSubjects);
// storing multilpe data types inside an array is not recommended. In a context of programming this not make any sense;
// An array should be a collecting of data that describe a similar/single topic/subject.
// Every individual piece of code/inforamtion is called property of an object
let details = ["keanu", "Reeves", 32, true];
console.log(details);

//  they are used to create complex data that containes pieces of information taht are relevant to each other.
// objects are another special kind of composite data type that is used to mimic or represent a real worl object/item.
let cellphone = {
	brand: "Samsung",
	model: "A12",
	color: "black",
	serialNo: "AX12002122",
	isHomeCredit: true,
	features: ["call", "texting", "Ringing", "5G"],
	price: 8000
};

console.log(cellphone.brand);
let personName = "Michael";
personName = "kobe"
console.log(personName);

// concatenate strings (+)
let pet = "dog";
console.log("this is the initial value of var: " + pet);

 pet = "cat";

console.log("this is the initial  new value of var: " + pet);

// const
const pi = 3.14;
console.log(pi);

const year = 12;
console.log(year);

const familyName = "Dela cruz";
console.log(familyName);


let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];


// console.log(firstName);
// console.log(lastName);
// console.log(age);
// console.log(hobbies);

 function getUserData () {
 	let person = {
 		FirstName: firstName,
 		LastName: lastName,
 		Age: age,
 		Hobbies: hobbies
 	}
 	return person;
 }

let result = getUserData();

console.log(result);